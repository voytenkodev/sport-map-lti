const SportAreas = require('../models/SportAreaModel')
const User = require('../models/f_UserModel')

module.exports = async(req, res) => {
    const sportareas = await SportAreas.find({})
    const user = await User.findById(req.session.userId)
    res.render('index', {
        sportareas, user
    });
}