const User = require('../models/f_UserModel')
const path = require('path')

module.exports = async(req, res) => {
    if (req.files == null) {
        await User.create({
            ...req.body,
            image: '/img/default.png'
        })
        res.redirect('/')
    } else {
        let image = req.files.image;
        image.mv(path.resolve(__dirname,'..','public/img/',image.name), 
        async(error)=>{
            User.create({
                ...req.body,
                image: '/img/' + image.name,
            });
            if(error) {
                const validationErrors = Object.keys(error.errors).map(key=>error.errors[key].message)
                req.flash('validationErrors', validationErrors)
                req.flash('data', req.body)
                return res.redirect('/auth/register')
            }
            res.redirect('/')
        })
        
    }
}
